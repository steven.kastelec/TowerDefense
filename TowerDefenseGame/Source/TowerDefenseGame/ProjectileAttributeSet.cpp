// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileAttributeSet.h"
#include "Net/UnrealNetwork.h"

UProjectileAttributeSet::UProjectileAttributeSet()
{

}

void UProjectileAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UProjectileAttributeSet, Lifetime, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UProjectileAttributeSet, BlastRadius, COND_None, REPNOTIFY_Always);
}

void UProjectileAttributeSet::OnRep_Lifetime(const FGameplayAttributeData& OldLifetime)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UProjectileAttributeSet, Lifetime, OldLifetime);
}

void UProjectileAttributeSet::OnRep_BlastRadius(const FGameplayAttributeData& OldBlastRadius)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UProjectileAttributeSet, BlastRadius, OldBlastRadius);

}