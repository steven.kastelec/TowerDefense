// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyController.h"

AEnemyController::AEnemyController()
{
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComp"));
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
}

void AEnemyController::OnPossess(APawn* InPawn) 
{
	Super::OnPossess(InPawn);
	
	AEnemyBaseCharacter* ThisCharacter = Cast<AEnemyBaseCharacter>(InPawn);

	if (ThisCharacter)
	{
		if (ThisCharacter->BotBehaviour)
		{ 
			RunBehaviorTree(ThisCharacter->BotBehaviour);
			UE_LOG(LogTemp,Warning,TEXT("Should start behaviour tree"))
		}
	}
}