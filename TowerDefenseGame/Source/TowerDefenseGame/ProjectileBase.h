// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbilitySystemInterface.h"
#include "PlayerAbilitySystemComponent.h"
#include "ProjectileAttributeSet.h"
#include "RepeaterGameplayAbility.h"
#include <GameplayEffectTypes.h>
#include "ProjectileBase.generated.h"

UCLASS()
class TOWERDEFENSEGAME_API AProjectileBase : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()

	/*ability system component*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UPlayerAbilitySystemComponent* AbilitySystemComponent;

	/*Player attribute class*/
	UPROPERTY()
	class UProjectileAttributeSet* Attributes;

public:	
	// Sets default values for this actor's properties
	AProjectileBase();

	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	virtual void InitializeAttributes();
	virtual void GiveAbilities();

	/*Effect that initializes default Attributes*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TSubclassOf<class UGameplayEffect> DefaultAttributeEffect;

	/*Effect that initializes default Abilities*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TArray<TSubclassOf<class URepeaterGameplayAbility>> DefaultAbilities;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
