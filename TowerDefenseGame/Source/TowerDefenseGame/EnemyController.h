// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "EnemyBaseCharacter.h"
#include "EnemyController.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSEGAME_API AEnemyController : public AAIController
{
	GENERATED_BODY()

	UPROPERTY(transient)
	TWeakObjectPtr<class UBlackboardComponent> BlackboardComp;

	UPROPERTY(transient)
	TWeakObjectPtr<class UBehaviorTreeComponent> BehaviorComp;


public:

	AEnemyController();
	
	virtual void OnPossess(APawn* InPawn) override;

};
