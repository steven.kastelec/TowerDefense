// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefenseGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TowerDefenseGame, "TowerDefenseGame" );
 