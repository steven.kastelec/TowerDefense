// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "ProjectileAttributeSet.generated.h"

//use custom macros from Attribute.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
    GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
    GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
    GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
    GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * 
 */
UCLASS()
class TOWERDEFENSEGAME_API UProjectileAttributeSet : public UAttributeSet
{
    GENERATED_BODY()

public:

    UProjectileAttributeSet();

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    /*=====================================================================================================*/
    //Define a Damage attribute, keeps track of Projectiles damage
    UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Lifetime)
    FGameplayAttributeData Lifetime;                                                                  //this defines the health attribute
    ATTRIBUTE_ACCESSORS(UProjectileAttributeSet, Lifetime);                                               //allows getters and setters
    
    UFUNCTION()
    virtual void OnRep_Lifetime(const FGameplayAttributeData& OldLifetime);

    /*=====================================================================================================*/
//Define a Damage attribute, keeps track of Projectiles damage
    UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_BlastRadius)
        FGameplayAttributeData BlastRadius;                                                                  //this defines the health attribute
    ATTRIBUTE_ACCESSORS(UProjectileAttributeSet, BlastRadius);                                               //allows getters and setters

    UFUNCTION()
        virtual void OnRep_BlastRadius(const FGameplayAttributeData& OldBlastRadius);
};
