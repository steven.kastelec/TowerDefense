// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbilitySystemInterface.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "PlayerAbilitySystemComponent.h"
#include "PlayerAttributeSet.h"
#include "RepeaterGameplayAbility.h"
#include <GameplayEffectTypes.h>
#include "TowerBase.generated.h"

UCLASS()
class TOWERDEFENSEGAME_API ATowerBase : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()

	/*ability system component*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UPlayerAbilitySystemComponent* AbilitySystemComponent;

	/*Player attribute class*/
	UPROPERTY()
	class UPlayerAttributeSet* Attributes;
	
public:	
	// Sets default values for this actor's properties
	ATowerBase();

	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* MyMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DetectArea)
	USphereComponent* DetectionSphere;

	virtual void InitializeAttributes();
	virtual void GiveAbilities();

	/*gameplay effect that initializes default Attributes*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TSubclassOf<class UGameplayEffect> DefaultAttributeEffect;

	/*list of abilities to initialize when actor begins*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TArray<TSubclassOf<class URepeaterGameplayAbility>> DefaultAbilities;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
