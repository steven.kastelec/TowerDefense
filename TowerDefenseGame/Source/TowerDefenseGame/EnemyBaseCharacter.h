// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "PlayerAbilitySystemComponent.h"
#include "PlayerAttributeSet.h"
#include "RepeaterGameplayAbility.h"
#include <GameplayEffectTypes.h>
#include "EnemyBaseCharacter.generated.h"

UCLASS()
class TOWERDEFENSEGAME_API AEnemyBaseCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

	/*ability system component*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UPlayerAbilitySystemComponent* AbilitySystemComponent;

	/*Player attribute class*/
	UPROPERTY()
	class UPlayerAttributeSet* Attributes;

public:
	// Sets default values for this character's properties
	AEnemyBaseCharacter();

	UPROPERTY(EditAnywhere, Category = Behaviour)
	class UBehaviorTree* BotBehaviour;

	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	virtual void InitializeAttributes();
	virtual void GiveAbilities();

	/*Effect that initializes default Attributes*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TSubclassOf<class UGameplayEffect> DefaultAttributeEffect;

	/*Effect that initializes default Abilities*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "GAS")
	TArray<TSubclassOf<class URepeaterGameplayAbility>> DefaultAbilities;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
