// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "PlayerAttributeSet.generated.h"

//use custom macros from Attribute.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
    GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
    GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
    GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
    GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * 
 */
UCLASS()
class TOWERDEFENSEGAME_API UPlayerAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	

public:

    UPlayerAttributeSet();

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    /*=====================================================================================================================================*/
    //Define a health attribute, keeps track of players health 
    UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Health)
    FGameplayAttributeData Health;                                                                  //this defines the health attribute
    ATTRIBUTE_ACCESSORS(UPlayerAttributeSet, Health);                                               //allows getters and setters

    UFUNCTION()
    virtual void OnRep_Health(const FGameplayAttributeData& OldHealth);
    
    /*=====================================================================================================================================*/
    //Define a Score attribute, used to keep track of currency 
    UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Score)
    FGameplayAttributeData Score;                                                                  //this defines the Score attribute
    ATTRIBUTE_ACCESSORS(UPlayerAttributeSet, Score);                                               //allows getters and setters

    UFUNCTION()
    virtual void OnRep_Score(const FGameplayAttributeData& OldScore);  //Define Function for Score

    /*=====================================================================================================================================*/
    /*Define a Clock attribute, used to define current clock time*/
    UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_CurrentAmmo)
        FGameplayAttributeData CurrentAmmo;                                                              //this defines the Damage attribute
    ATTRIBUTE_ACCESSORS(UPlayerAttributeSet, CurrentAmmo);                                               //allows getters and setters

    UFUNCTION()
        virtual void OnRep_CurrentAmmo(const FGameplayAttributeData& OldCurrentAmmo);  //Define Function for Damage

   /*=====================================================================================================================================*/
    UPROPERTY(BlueprintReadOnly, Category = "Attributes", ReplicatedUsing = OnRep_Lives)
    FGameplayAttributeData Lives;                                                              //this defines the Lives attribute
    ATTRIBUTE_ACCESSORS(UPlayerAttributeSet, Lives);                                               //allows getters and setters

    UFUNCTION()
    virtual void OnRep_Lives(const FGameplayAttributeData& OldLives);  //Define Function for Lives

};
