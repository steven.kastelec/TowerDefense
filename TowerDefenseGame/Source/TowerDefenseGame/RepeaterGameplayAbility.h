// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "TowerDefenseGame.h"
#include "RepeaterGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSEGAME_API URepeaterGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	
	URepeaterGameplayAbility();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	EAbilityInputID AbilityInputID = EAbilityInputID::None;
	
};
